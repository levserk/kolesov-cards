import solid from "solid-start/vite";
import { defineConfig } from "vite";
import viteImagemin from "vite-plugin-imagemin";
import { navigation } from "./src/const/navigation";

const GOODS = [
  "bronetehnika-sssr-i-germanii-vtoroj-mirovoj-vojny",
  "bronetehnika-germanii-vtoroj-mirovoj-vojny",
  "bronetehnika-yaponii-vtoroj-mirovoj-vojny",
  "bronetehnika-SSSR-vtoroj-mirovoj-vojny",
  "bronetehnika-ssha-vtoroj-mirovoj-vojny",
  "bronetehnika-velikobritanii-vtoroj-mirovoj-vojny",
  "bronetehnika-francii-vtoroj-mirovoj-vojny",
  "bronetehnika-italii-vtoroj-mirovoj-vojny",
  "politiki-20-veka",
  "kultovye-kinogeroi",
  "parusniki",
  "parusniki",
  "aviaciya-sssr",
  "chehol-dlya-igralnyh-kart",
  "znachok-kollekcioner-igralnyh-kart-1",
  "znachok-kollekcioner-igralnyh-kart-2",
  "znachok-kollekcioner-igralnyh-kart-3",
  "monarhi-20-veka",
];

const routes = Object.values(navigation);
const goodsRoutes = GOODS.map((id) => `${navigation.catalog}/${id}`);

export default defineConfig({
  plugins: [
    solid({
      adapter: "solid-start-static",
      prerenderRoutes: [...routes, ...goodsRoutes, "/test"],
    }),
    viteImagemin({
      mozjpeg: {
        quality: 60,
      },
    }),
  ],
});
