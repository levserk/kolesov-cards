import { Show, createEffect, createSignal } from "solid-js";
import { useParams } from "solid-start";

import Delimiter from "~/components/Delimiter";
import { LineNavigation } from "~/components/LineNavigation";
import { GoodsRow } from "../components/GoodsRow";
import { Page } from "../components/Page";
import { GOODS, GoodType } from "../const/goods";

const title =
  "Каталог игральных карт Колесова. Авторские образовательные и пропагандистские игральные карты Колесова купить";
const description =
  "Уникальные авторские игральные карты. Для коллекционеров и интересующихся историей. Купить игральные Колесова";

export function Product() {
  const params = useParams();
  const [good, setGood] = createSignal<GoodType | undefined>(
    GOODS.find((g) => g.id === params.id)
  );
  const [currentImage, setCurrentImage] = createSignal(0);

  createEffect(() => {
    setGood(GOODS.find((g) => g.id === params.id));
  });

  function onImageClick(e: { currentTarget: HTMLImageElement | null }) {
    const id = parseInt(e?.currentTarget?.getAttribute("data-id") || "0");
    setCurrentImage(id);
  }

  return (
    <Page
      title={good()?.pageTitle || title}
      description={good()?.metaDescription || description}
    >
      <Show
        when={good() !== undefined}
        fallback={<div>Товар не найден...</div>}
      >
        <LineNavigation
          links={[
            { title: "Главная", url: "/" },
            { title: "Каталог", url: "/igralnye-karty-Kolesova" },
            {
              title: good()?.title,
              url: `/igralnye-karty-Kolesova/${good()?.id}`,
            },
          ]}
        />
        <div class="text-center font-bold text-2xl whitespace-pre-line">
          {good()?.title}
        </div>
        <div class="flex flex-col sm:flex-row gap-3">
          <div class="flex flex-col sm:w-6/12">
            <img
              class="w-full object-contain"
              src={good()?.images[currentImage()]}
              alt={good()?.altImages[currentImage()] || ""}
            />
            <div class="flex flex-row gap-x-3 pt-3">
              {good()?.images.map((image, i) => (
                <div class={`flex-grow flex justify-center `}>
                  <img
                    class={`w-full object-cover cursor-pointer box-content border ${
                      i == currentImage() ? `shadow-xl border-gray-400` : ``
                    }`}
                    src={image}
                    alt={good()?.altImages[i] || ""}
                    onClick={onImageClick}
                    data-id={i}
                  />
                </div>
              ))}
            </div>
          </div>
          <div class="flex flex-col sm:w-6/12 md:pl-4 ">
            <span class="text-sm text-justify indent-2.5 whitespace-pre-line">
              {good()?.description}
            </span>
          </div>
        </div>

        <Show when={good()?.videoLink !== undefined}>
          <div class="flex flex-row justify-center my-10">
            <iframe
              width="768"
              height="432"
              src={good()?.videoLink}
              title="YouTube video player"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowfullscreen
            ></iframe>
          </div>
        </Show>

        <div class="text-justify indent-2.5 text-md whitespace-pre-line">
          {good()
            ?.fullDescription.split("\n")
            .filter((s) => s.trim().length > 0)
            .map((p) => (
              <p class=" min-h-[1rem] not-last:mb-4">{p}</p>
            ))}
        </div>

        <Delimiter />

        <div class="">
          <GoodsRow filterId={good()?.id} />
        </div>
      </Show>
    </Page>
  );
}
