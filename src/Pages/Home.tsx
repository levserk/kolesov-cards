import Delimiter from "~/components/Delimiter";
import { ArticlesList } from "../components/ArticlesList";
import { Baner } from "../components/Baner";
import { Goods } from "../components/Goods";
import { GoodsRow } from "../components/GoodsRow";
import { HelloMessage } from "../components/HelloMessage";
import { Page } from "../components/Page";

const title =
  "Авторские игральные карты Михаила Колесова. Образовательные и пропагандистские игральные карты";
const description =
  "Уникальные авторские игральные карты из Петербурга. Образовательные и пропагандистские игральные карты Колесова";

export default function Home() {
  return (
    <Page title={title} description={description}>
      <Baner />
      <HelloMessage />
      <Delimiter/>
      <GoodsRow />
      <ArticlesList />
    </Page>
  );
}
