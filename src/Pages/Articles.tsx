import { LineNavigation } from "~/components/LineNavigation";
import { ArticlesList } from "../components/ArticlesList";
import { Page } from "../components/Page";

const title = "Статьи на исторические и познавательные темы. Это интересно!";
const description =
  "Что почитать человеку, увлечённому историей? Авторские статьи на исторические и познавательные темы. Легко и интересно читать!";

export function Articles() {
  return (
    <Page title={title} description={description}>
      <LineNavigation links={[
        { title: "Главная", url: "/" },
        { title: "Статьи", url: "/avtorskie-teksty-kolesova" },
      ]} />
      <ArticlesList />
    </Page>
  );
}
