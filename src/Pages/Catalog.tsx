import { LineNavigation } from "~/components/LineNavigation";
import { Goods } from "../components/Goods";
import { Page } from "../components/Page";

const title =
  "Каталог игральных карт Колесова. Авторские образовательные и пропагандистские игральные карты Колесова купить";
const description =
  "Уникальные авторские игральные карты. Для коллекционеров и интересующихся историей. Купить игральные Колесова";

export function Catalog() {
  return (
    <Page title={title} description={description}>
      <LineNavigation
        links={[
          { title: "Главная", url: "/" },
          { title: "Каталог", url: "/igralnye-karty-Kolesova" },
        ]}
      />

      <div class="text-center font-bold text-2xl">
        Каталог образовательных и пропагандистских карт
      </div>
      <div class="mx-auto text-justify indent-2.5 text-lg [&>:not(:last-child)]:pb-2">
        <p>
          Здесь собраны все выпущенные колоды игральных карт Колесова с
          подробным описанием, характеристиками и фотографиями. Если вы хотите
          купить образовательные игральные карты Колесова, свяжитесь со мной
          любым удобным для Вас способом.
        </p>
      </div>
      <Goods />
    </Page>
  );
}
