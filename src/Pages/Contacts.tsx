import { LineNavigation } from "~/components/LineNavigation";
import { Page } from "../components/Page";

const title = "Связаться с автором игральных карт Колесова. Контактные данные";
const description =
  "Как связаться с автором игральных карт Колесова? Задать вопрос автору";

export function Contacts() {
  return (
    <Page title={title} description={description}>
      <LineNavigation
        links={[
          { title: "Главная", url: "/" },
          { title: "Контакты", url: "/kontakty" },
        ]}
      />

      <div class="font-bold text-2xl whitespace-pre-line pt-3">
        Контактные данные автора и издателя Михаила Колесова
      </div>

      <div class="mx-auto text-justify indent-0 text-lg py-4 [&>*]:py-1 [&>*]:text-left">
        <p>Россия, Санкт-Петербург</p>
        <p>
          телефон:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="tel:+79213150012"
          >
            +79213150012
          </a>
        </p>
        <p>
          e-mail:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="mailto:makol@bk.ru"
          >
            makol@bk.ru
          </a>
        </p>
        <p>
          личная страницы в vk:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://vk.com/makolesov"
            target="_blank"
          >
            https://vk.com/makolesov
          </a>
        </p>
        <p>
          группа в vk «Игральные карты Колесова»:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://vk.com/kolesovcards"
            target="_blank"
          >
            https://vk.com/kolesovcards
          </a>
        </p>
        <p>
          группа в vk «Реплики исторических изделий»:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://vk.com/replika20vek"
            target="_blank"
          >
            https://vk.com/replika20vek
          </a>
        </p>
        <p>
          Telegram-канал:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://t.me/kolesovcards"
            target="_blank"
          >
            https://t.me/kolesovcards
          </a>
        </p>
        <p>
          YouTube:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://www.youtube.com/@kolesovcards"
            target="_blank"
          >
            https://www.youtube.com/@kolesovcards
          </a>
        </p>
        <p>
          канал яндекс-дзен:{" "}
          <a
            class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://zen.yandex.ru/grafoman"
            target="_blank"
          >
            https://zen.yandex.ru/grafoman
          </a>
        </p>
        <div class="flex flex-col gap-y-1.5 mt-6">
          <b>Как купить игральные карты Колесова?</b>
          <ul>
            <li>1. Связаться с автором любым удобным для Вас способом</li>
            <li>
              2. На маркетплейсах OZON и Яндекс-маркет. Ищутся карты легко – по
              запросу «Игральные карты Колесова»
            </li>
          </ul>
          <p>Для оптовых покупателей цена индивидуальная.</p>
        </div>
      </div>
    </Page>
  );
}
