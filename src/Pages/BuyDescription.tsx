import { LineNavigation } from "~/components/LineNavigation";
import { Page } from "../components/Page";

const title = "Как и где можно купить игральные карты Колесова";
const description =
  "Как и где можно купить игральные карты Колесова? Как заказать, оплатить и получить заказ";

export function BuyDescription() {
  return (
    <Page title={title} description={description}>
      <LineNavigation
        links={[
          { title: "Главная", url: "/" },
          { title: "Как купить", url: "/kak-kupit-karty-kolesova" },
        ]}
      />

      <div class="mx-auto text-lg text-left">
        <ol class="list-decimal py-4 [&>*]:py-2 ml-4 text-left">
          <li>выбрать интересующий товар</li>
          <li>
            определиться со способом получения товара (личная встреча в
            Санкт-Петербурге, отправка Почтой РФ или СДЭКом)
          </li>
          <li>
            <p>cвязаться со мной любым удобным для Вас способом:</p>
            <p>
              - написать{" "}
              <a
                class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                href="mailto:makol@bk.ru"
              >
                makol@bk.ru
              </a>
            </p>
            <p>
              - позвонить{" "}
              <a
                class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                href="tel:+79213150012"
              >
                +79213150012
              </a>
            </p>
            <p>
              - написать личное сообщение на мою страницу{" "}
              <a
                class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                href="https://vk.com/makolesov"
                target="_blank"
              >
                https://vk.com/makolesov
              </a>
            </p>
            <p>
              - написать личное сообщение в любую из моих групп{" "}
              <a
                class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                href="https://vk.com/kolesovcards"
                target="_blank"
              >
                https://vk.com/kolesovcards
              </a>{" "}
              или{" "}
              <a
                class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
                href="https://vk.com/replika20vek"
                target="_blank"
              >
                https://vk.com/replika20vek
              </a>
            </p>
          </li>
          <li>оплатить товар и доставку</li>
          <li>получить товар и радоваться покупке :)</li>
        </ol>
        <p>
          Отправка товара осуществляется по предоплате, оплата товара при личной
          встрече происходит при получении.
        </p>
        <p>
          Также игральные карты Колесова можно купить на маркетплейсах OZON и
          Яндекс-маркет. Ищутся они легко – по запросу «Игральные карты
          Колесова»
        </p>
        <p>Для оптовых покупателей цена конечно же индивидуальная.</p>
      </div>
    </Page>
  );
}
