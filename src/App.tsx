import type { Component } from "solid-js";
import { Route, Routes } from "solid-start";
import { Footer } from "./components/Footer";
import Header from "./components/Header";
import { navigation } from "./const/navigation";
import { About } from "./Pages/About";
import { Articles } from "./Pages/Articles";
import { Catalog } from "./Pages/Catalog";
import { Contacts } from "./Pages/Contacts";
import Home from "./Pages/Home";
import { Product } from "./Pages/Product";

const App: Component = () => {
  return (
    <div class="flex flex-col min-h-screen">
      <Header />
      <Routes>
        <Route path={navigation.home} component={Home} />
        <Route path={navigation.about} component={About} />
        <Route path={navigation.catalog} component={Catalog} />
        <Route path={`${navigation.catalog}/:id`} component={Product} />
        <Route path={navigation.articles} component={Articles} />
        <Route path={navigation.contacts} component={Contacts} />
        {/*<Route path={navigation.buy} component={BuyDescription} />*/}
      </Routes>
      <Footer />
    </div>
  );
};

export default App;
