// @refresh reload
import {
  Body,
  ErrorBoundary,
  Head,
  Html,
  Link,
  Meta,
  Route,
  Routes,
  Scripts,
  Title,
} from "solid-start";
import App from "./App";
import favicon from "./assets/favicon/favicon.ico";
import "./root.css";
import { TestApp } from "./test-app/TestApp";
import { Yandex } from "./yandex";

export default function Root() {
  //const location = useLocation();
  //console.log(location.pathname);

  return (
    <Html lang="en">
      <Head>
        <Title>
          Авторские игральные карты Михаила Колесова. Образовательные и
          пропагандистские игральные карты
        </Title>
        <Meta charset="utf-8" />
        <Meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta
          name="description"
          content="Уникальные авторские игральные карты из Петербурга. Образовательные и пропагандистские игральные карты Колесов"
        />
        <Meta name="theme-color" content="#000000" />
        <Meta name="yandex-verification" content="bbe646dd339722fd" />
        <Meta
          name="google-site-verification"
          content="uVync1TGMhkzFlOwf28k7t7W23N0wl5OZLLzaD6jkP8"
        />
        <Link rel="shortcut icon" type="image/ico" href={favicon} />
      </Head>
      <Body>
        <ErrorBoundary>
          <Routes>
            <Route path="/test" component={TestApp} />
            <Route path="/*" component={App} />
          </Routes>
        </ErrorBoundary>
        <Scripts />
        <Yandex />
      </Body>
    </Html>
  );
}
