import { For } from "solid-js";
import { Page } from "../components/Page";
import bridge from "@vkontakte/vk-bridge";
import { isServer } from "solid-js/web";

const title = "Тест по технике второй мировой";
const description = "Тест по технике второй мировой";

const Question = {
  variants: [
    `1. Средний танк Pz.Kpfw. V "Panther" Ausf. A`,
    `2. Средний танк Pz.Kpfw. IV Ausf. J`,
    `3. Средний танк Pz.Kpfw. V "Panther" Ausf. G`,
    `4. Средний танк Pz.Kpfw. IV Ausf. G`,
  ],
  answer: 3,
};

try {
  // Отправляет событие инициализации нативному клиенту
  bridge.send("VKWebAppInit");
} catch (e) {
  if (!isServer) {
    console.error(e);
  }
}

function Button(props) {
  const title = props.title;
  return (
    <button class="flex p-5 border-solid border-2 border-slate-300 rounded-md mb-5">
      {title}
    </button>
  );
}

export function TestApp() {
  return (
    <Page title={title} description={description}>
      <h1 class="mx-auto text-center text-2xl my-5 text-gray-700">
        Выберите правильный вариант ответа
      </h1>
      <div class="mx-auto flex justify-center items-center my-10">
        <img
          src={new URL(`./assets/${"Pantera_G"}.jpg`, import.meta.url).href}
        />
      </div>
      <div class="mx-auto flex flex-col justify-center items-center">
        <div class="inline-flex flex-col justify-start items-stretch">
          <For each={Question.variants}>
            {(variant) => <Button title={variant} />}
          </For>
        </div>
      </div>
    </Page>
  );
}
