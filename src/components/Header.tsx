import { NavLink } from "@solidjs/router";
import { createEffect, createSignal, Show } from "solid-js";
import { navigation } from "../const/navigation";

function Links() {
  return (
    <>
      <NavLink href={navigation.about} activeClass="text-red-300">
        о проекте
      </NavLink>
      <NavLink href={navigation.catalog} activeClass="text-red-300">
        каталог
      </NavLink>
      <NavLink href={navigation.articles} activeClass="text-red-300">
        статьи
      </NavLink>
      <NavLink href={navigation.contacts} activeClass="text-red-300">
        контакты
      </NavLink>
    </>
  );
}

export default function Header() {
  const [showMenu, setShowMenu] = createSignal(false);

  return (
    <header class="bg-gray-700 text-white" onClick={() => setShowMenu(false)}>
      <div class="mx-auto text-center text-2xl py-5 bg-yellow-100 text-gray-700">
        <h1>
          <NavLink href={navigation.home}>
            Игральные карты Михаила Колесова
          </NavLink>
        </h1>
      </div>
      <div class="mx-auto max-w-3xl py-2">
        <span
          class="flex sm:hidden text-2xl ml-4"
          onClick={(e) => setShowMenu(!showMenu()) && e.stopPropagation()}
        >
          ☰
        </span>
        <nav class="header hidden sm:flex flex-row justify-around gap-4 text-lg flex-wrap md:flex-nowrap">
          <Links />
        </nav>
      </div>
      <Show when={showMenu()}>
        <div class="flex sm:hidden fixed inset-0 bg-black/10 backdrop-blur-sm z-10">
          <div class="flex sm:hidden bg-white absolute w-60 h-full z-10 p-8">
            <nav class="flex flex-col justify-start gap-6 text-xl text-gray-800">
              <Links />
            </nav>
          </div>
        </div>
      </Show>
    </header>
  );
}
