import { For } from "solid-js";
import ok from "../assets/social/Ok.png";
import telegram from "../assets/social/Telegram.png";
import vk from "../assets/social/VK.png";
import youtube from "../assets/social/YouTube.png";
import zen from "../assets/social/Zen.png";
/*
https://vk.com/kolesovcards
https://www.youtube.com/channel/UC6kFcd-NFpGZ4zy5-ToU9Aw
https://dzen.ru/grafoman
https://ok.ru/group/70000000768631
*/

export const SocialLinks = [
  {
    src: vk,
    link: "https://vk.com/kolesovcards",
  },
  {
    src: ok,
    link: "https://ok.ru/group/70000000768631",
  },
  {
    src: youtube,
    link: "https://www.youtube.com/channel/UC6kFcd-NFpGZ4zy5-ToU9Aw",
  },
  {
    src: zen,
    link: "https://dzen.ru/grafoman",
  },
  {
    src: telegram,
    link: "https://t.me/kolesovcards",
  },
];

export function SocialIcons() {
  return (
    <div class="flex flex-row justify-left space-x-2">
      <For each={SocialLinks}>
        {(social) => (
          <a href={social.link} class="" target="_blank">
            <img src={social.src} class="w-7" />
          </a>
        )}
      </For>
    </div>
  );
}
