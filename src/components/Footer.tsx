import { SocialIcons } from "./SocialIcons";

export function Footer() {
  return <header class="bg-gray-700 text-white py-2">
    <div class="mx-auto max-w-4xl">
      <div class="flex flex-row justify-center space-x-8 md:space-x-16 text-lg" >
        <div class="md:w-1/3 grow-1"></div>
        <div class="flex flex-row justify-center">© 2022 KolesovCards</div>
        <div class="md:w-1/3 grow-1"><SocialIcons/></div>
      </div>
    </div>
  </header>
}