import { NavLink } from "@solidjs/router";
import { For, Show } from "solid-js";

interface Props {
  links: {
    url: string;
    title: string;
  }[]
}

export function LineNavigation({ links }: Props) {
  return <div class="flex flex-row text-sm">
    <For each={links}>
      {(link, index) => <NavLink href={link.url} class="flex flex-row items-end mr-0.5 max-w-[70%]">
        <Show when={index() > 0} >
          <span class="mr-0.5 text-gray-500">
            {"/"}
          </span>
        </Show>
        <span class="whitespace-nowrap truncate">{link.title}</span>
      </NavLink>}
    </For>
  </div>
}