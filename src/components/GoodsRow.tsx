import { createEffect, createSignal, For } from "solid-js";
import { GOODS, GoodType } from "../const/goods";
import { GoodsPreview } from "./GoodsPreview";

let container: HTMLDivElement | undefined;

const onClickLeft = () => {
  if (!container) {
    return;
  }
  let allColumns = Array.from(container.children) as HTMLElement[];
  const allWidth = container.offsetWidth;
  const scroll = container.scrollLeft;
  allColumns = allColumns.filter(
    (col) => col.offsetLeft + col.offsetWidth < scroll
  );

  if (allColumns.length) {
    let offset = allColumns[allColumns.length - 1].offsetLeft;
    container.scrollLeft = offset - 8;
  }
};

const onClickRight = () => {
  if (!container) {
    return;
  }
  let allColumns = Array.from(container.children) as HTMLElement[];
  const allWidth = container.offsetWidth;
  const scroll = container.scrollLeft;
  allColumns = allColumns.filter(
    (col) => col.offsetLeft + col.offsetWidth > scroll + allWidth
  );

  if (allColumns.length) {
    container.scrollLeft = allColumns[0].offsetLeft - allWidth + allColumns[0].offsetWidth + 8;
  }
};

interface Props {
  filterId?: string;
}

export function GoodsRow(props: Props) {
  const [goods, setGoods] = createSignal<GoodType[]>(GOODS);
  createEffect(() => {
    console.log(props.filterId);
    setGoods(GOODS.filter((good) => good.id !== props.filterId));
  });

  return (
    <div>
      <div class="relative">
        <span
          onClick={onClickLeft}
          class={`absolute top-1/2 transform -translate-y-1/2 rounded-full -left-0 lg:-left-8 h-8 w-8 
          bg-center bg-cover bg-no-repeat bg-leftArrow opacity-60 hover:opacity-100 cursor-pointer flex items-center justify-center text-black text-2xl`}></span>
        <div
          ref={container}
          class="flex flex-row gap-4 items-stretch p-2 overflow-x-scroll scroll-smooth scrollbar-hide">
          <For each={goods()}>{(good) => <GoodsPreview good={good} />}</For>
        </div>
        <span
          onClick={onClickRight}
          class={`absolute top-1/2 transform -translate-y-1/2 rounded-full right-0 lg:-right-8 h-8 w-8 
          bg-center bg-cover bg-no-repeat bg-rightArrow opacity-60 hover:opacity-100 cursor-pointer flex items-center justify-center text-black text-2xl`}></span>
      </div>
    </div>
  );
}
/*let allColumns = Array.from(tr.querySelectorAll("th"));
        const allWidth = ref.current.offsetWidth;
        const scroll = ref.current.scrollLeft;
        allColumns = allColumns.filter(
          (col) => col.offsetLeft + col.offsetWidth > scroll + allWidth
        );
        if (allColumns.length) {
          ref.current.scrollLeft =
            allColumns[0].offsetLeft - allWidth + allColumns[0].offsetWidth;
        }*/
