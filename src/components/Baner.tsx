import { createEffect, createSignal, onCleanup } from "solid-js";

import baner_sssr from "../assets/baners/1.jpg";
import baner_kino from "../assets/baners/10.jpg";
import sailboats from "../assets/baners/11.jpg";
import baner_monarhs from "../assets/baners/12.jpg";
import baner_germany from "../assets/baners/2.jpg";
import baner_japan from "../assets/baners/3.jpg";
import baner_usa from "../assets/baners/4.jpg";
import baner_sssr_germany from "../assets/baners/5.jpg";
import baner_england from "../assets/baners/6.jpg";
import baner_france from "../assets/baners/7.jpg";
import baner_italy from "../assets/baners/8.jpg";
import baner_politics from "../assets/baners/9.jpg";

const baners = [
  baner_monarhs,
  sailboats,
  baner_sssr,
  baner_germany,
  baner_japan,
  baner_usa,
  baner_england,
  baner_france,
  baner_italy,
  baner_sssr_germany,
  baner_politics,
  baner_kino,
];
const links = [
  "/igralnye-karty-Kolesova/monarhi-20-veka",
  "/igralnye-karty-Kolesova/parusniki",
  "/igralnye-karty-Kolesova/bronetehnika-SSSR-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-germanii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-yaponii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-ssha-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-velikobritanii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-francii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-italii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/bronetehnika-sssr-i-germanii-vtoroj-mirovoj-vojny",
  "/igralnye-karty-Kolesova/politiki-20-veka",
  "/igralnye-karty-Kolesova/kultovye-kinogeroi",
];
const [getCurrentBanner, setCurrentBaner] = createSignal(0);

function setNextBaner() {
  const current = getCurrentBanner();
  if (current < baners.length - 1) {
    setCurrentBaner(current + 1);
  } else {
    setCurrentBaner(0);
  }
}

export function Baner() {
  let interval: NodeJS.Timer;

  createEffect(() => {
    interval = setInterval(() => {
      setNextBaner();
    }, 7000);
  });

  onCleanup(() => {
    if (interval) {
      clearInterval(interval);
    }
  });

  return (
    <div class="py-1">
      <a href={links[getCurrentBanner()]}>
        <img src={baners[getCurrentBanner()]} />
      </a>
    </div>
  );
}
