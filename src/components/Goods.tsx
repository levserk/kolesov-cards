import { For } from "solid-js";
import { GOODS } from "../const/goods";
import { GoodsPreview } from "./GoodsPreview";

export function Goods() {
  return (
    <div class="flex flex-row flex-wrap items-stretch gap-4 justify-around md:justify-start py-5">
      <For each={GOODS}>{(good) => <GoodsPreview good={good} />}</For>
    </div>
  );
}
