import { NavLink } from "@solidjs/router";
import { GoodType } from "../const/goods";
import { navigation } from "../const/navigation";

interface Props {
  good: GoodType;
}

export function GoodsPreview({ good }: Props) {
  return (
    <div class="w-44">
      <NavLink
        href={`${navigation.catalog}/${good.id}`}
        class="h-full flex flex-col overflow-hidden shadow-xl rounded-lg border border-gray-300"
      >
        <img class="w-44" src={good.src} />
        <div class="w-44 p-3 flex-grow flex flex-col justify-between">
          <span class="text-sm text-left">{good.title}</span>
        </div>
      </NavLink>
    </div>
  );
}
