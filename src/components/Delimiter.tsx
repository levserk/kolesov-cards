export default function Delimiter() {
  return (
    <section>
      <div class="h-5 bg-delimeter bg-center bg-contain bg-no-repeat my-2" />
    </section>
  );
}
