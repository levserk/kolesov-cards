import { For } from "solid-js";

import img2 from "../assets/jargon.jpg";
import img1 from "../assets/kak-festivali.jpg";

interface ArticleType {
  src: string;
  title: string;
  link: string;
}

const ARTICLES = [
  {
    src: img1,
    title:
      "Как фестивали реконструкторов получаются круче и правдивей фильмов от Минкульта",
    link: "https://zen.yandex.ru/a/XiV5EEP9wACtZGc8?",
  },
  {
    src: img2,
    title:
      "Жаргон немецких солдат Второй мировой войны. Пердолов, шейная болячка и спаржа Роммеля. Что это?",
    link: "https://zen.yandex.ru/a/YEM8fmZeRBPzeGKB?&",
  },
];

interface Props {
  article: ArticleType;
}

function Article({ article }: Props) {
  return (
    <a
      href={article.link}
      class="flex flex-row overflow-hidden shadow-xl rounded-lg border border-gray-300"
    >
      <img class="w-44" src={article.src} />
      <span class=" w-44 text-sm font-bold p-4 pt-1">{article.title}</span>
    </a>
  );
}

export function ArticlesList() {
  return (
    <div class="py-5">
      <div class="text-center font-bold text-2xl">Это интересно!</div>
      <div class="flex flex-row flex-wrap md:flex-nowrap gap-4 justify-between py-5">
        <For each={ARTICLES}>{(article) => <Article article={article} />}</For>
      </div>
    </div>
  );
}
