import { createEffect, JSX } from "solid-js";

interface Props {
  children: JSX.Element;
  title: string;
  description: string;
}

export function Page(props: Props) {
  createEffect(() => {
    document.title = props.title;
    document
      .querySelector('meta[name="description"]')
      ?.setAttribute("content", props.description);
  });

  return (
    <section class="mx-auto max-w-3xl w-full flex-grow px-2 md:px-0 [&>*]:pt-3 [&>*]:pb-0 [&>:last-child]:pb-3" >
      {props.children}
    </section>
  );
}
