import { sssrAvia } from "./goods/avia/sssr_aiva.ts/sssr_avia";
import { badge_1 } from "./goods/badges/badge_1";
import { badge_2 } from "./goods/badges/badge_2";
import { badge_3 } from "./goods/badges/badge_3";
import { caseForCards } from "./goods/case/cards_case/cards_case";
import { cultMovieCharacters } from "./goods/characters/cultMovieCharacters";
import { monarhs } from "./goods/characters/monarhs";
import { politics } from "./goods/characters/politics";
import { england } from "./goods/england";
import { france } from "./goods/france";
import { germany } from "./goods/germany";
import { germany_and_sssr } from "./goods/germany_and_sssr";
import { italy } from "./goods/italy";
import { japan } from "./goods/japan";
import { sailboats } from "./goods/sailboats";
import { sssr } from "./goods/sssr";
import { usa } from "./goods/usa";

export interface GoodType {
  id: string;
  src: string;
  images: string[];
  altImages: string[];
  title: string;
  pageTitle: string;
  description: string;
  fullDescription: string;
  metaDescription: string;
  videoLink?: string;
  cost: number;
}

export const GOODS: GoodType[] = [
  sssr,
  germany,
  japan,
  usa,
  england,
  france,
  italy,
  germany_and_sssr,
  politics,
  cultMovieCharacters,
  sailboats,
  sssrAvia,
  monarhs,
  caseForCards,
  badge_1,
  badge_2,
  badge_3,
];
