type PageTitle = {
  title: string;
  description: string;
};

export const pageTitles: { [key: string]: PageTitle } = {
  "/": {
    title: "",
    description: "",
  },
};
