import foto1 from "../../../assets/cards/cultMovieCharacters/1.jpg";
import foto2 from "../../../assets/cards/cultMovieCharacters/2.jpg";
import foto3 from "../../../assets/cards/cultMovieCharacters/3.jpg";
import foto4 from "../../../assets/cards/cultMovieCharacters/4.jpg";
import foto5 from "../../../assets/cards/cultMovieCharacters/5.jpg";

export const cultMovieCharacters = {
  id: "kultovye-kinogeroi",
  src: foto1,
  images: [foto1, foto2, foto3, foto4, foto5],
  altImages: [
    "Игральные карты Колесова Культовые киногерои в масках и гриме",
    "Игральные карты Культовые киногерои",
    "Культовые киногерои",
    "Игральные карты Любимые киногерои",
    "Игральные карты Колесова Киногерои",
  ],
  videoLink: "https://www.youtube.com/embed/fwN-l-TkaRY?si=xrO30qXa6cMWLvXT",
  title: "Игральные карты\nКультовые киногерои в масках и гриме",
  pageTitle:
    "54 игральные карты с фото культовых киногероев, их именами и прозвищами, названиями фильмов, фото и ФИО сыгравших их актёров",
  metaDescription:
    "Авторская колода игральных карт, посвящённая культовым киногероям. 54 игральные карты с любимыми киногероями в масках и сложном гриме и сыгравшими их актёрами",
  description: `Авторские игральные карты "Культовые киногерои в масках и гриме"

  Колода содержит 54 игральные карты. На каждой карте размещены: 

  - фото культовых киногероев 
  - их имена и прозвища 
  - названия фильмов 
  - фото и ФИО актёров, исполнивших роли этих киногероев

  Карты напечатаны на картоне 300 г/м2, размер карт 57*89 мм. 

  Автор и издатель - Михаил Колесов
  
  Дизайн карт, идея колоды и общий вид всех составляющих колоды нотариально удостоверены, авторство подтверждено
  `,
  fullDescription: `Психопаты, отморозки и проститутка-убийца. Кровожадные фермеры, вампиры, инопланетяне, синие женщины и жестокий положенец. Мастера восточных единоборств, сердитая малолетняя девочка и сотрудник полиции, который вынужден работать даже после смерти. Генерал, который раньше был обезьяной; совершивший фатальную ошибку учёный, роботы и отец человека с забавным для русского уха именем "Люк". Сказочные существа и супергерои, злой клоун и красивая девушка, оказавшаяся ведьмой.
  Нет! Это не впечатление от прогулки по спальному району! Это персонажи из фильмов, присутствующие в колоде "Культовые киногерои в масках и гриме"!
  Герои любимых фильмов - подарок подарок киноману!
  Для тех, кто любит и помнит фильмы и эпоху VHS! Любимые киногерои! Отличный подарок для киномана, оригинальный презент для любителя фильмов, для коллекционера, ценное приобретение для тех, кто любит культовые фильмы!
  `,
  cost: 900,
};
