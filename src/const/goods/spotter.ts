import spotter_card_image from "../../assets/cards/spotter.jpg";
import spotter1 from "../../assets/cards/spotter/1.jpg";
import spotter2 from "../../assets/cards/spotter/2.jpg";
import spotter3 from "../../assets/cards/spotter/3.jpg";
import spotter4 from "../../assets/cards/spotter/4.jpg";

export const spotter = {
  id: "spotter",
  src: spotter_card_image,
  images: [spotter_card_image, spotter1, spotter2, spotter3, spotter4],
  altImages: [],
  title: "Игральные карты\nSpotter cards репринт колоды 1943 года",
  pageTitle: "",
  metaDescription: "",
  description: "",
  fullDescription: ``,
  cost: 900,
};
