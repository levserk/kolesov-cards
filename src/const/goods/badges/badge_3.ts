import { nbsp } from "~/const/nbsp";
import img1 from "../../../assets/badges/badge_3/3-1.jpg";
import img2 from "../../../assets/badges/badge_3/3-2.png";
import img3 from "../../../assets/badges/badge_3/3-3.png";
import img4 from "../../../assets/badges/badge_3/3-4.png";
import img5 from "../../../assets/badges/badge_3/3-5.png";

export const badge_3 = {
  id: "znachok-kollekcioner-igralnyh-kart-3",
  src: img1,
  images: [img1, img2, img3, img4, img5],
  altImages: [
    "Значок Коллекционер игральных карт",
    "коллекционер игральных карт",
    "Значок смола",
    "Значок крепление цанга",
    "Знак Коллекционер",
  ],
  title: `Значок "Коллекционер игральных карт" вид${nbsp}3`,
  pageTitle: "Значки на тему игральных карт. Выпущены малым тиражом",
  metaDescription:
    "Где купить значок на тему игральных карт? Коллекционирование игральных карт",
  description: `Диаметр 25 мм, толщина 3 мм 
  Материал - пластик + заливка полимерной смолой, крепление - латунная цанга 
  Малотиражное петербургское производство 
  
  Надписи на значке: 
  - Коллекционер игральных карт 
  - Trahit sua quemque voluptas - Всякого влечёт своя страсть (лат.) - девиз коллекционеров игральных карт, выбранный путём голосования в среде коллекционеров большинством голосов 
  
  Значки выпущены автором и издателем игральных карт Михаилом Колесовым
  `,
  fullDescription: ``,
  cost: 900,
};
